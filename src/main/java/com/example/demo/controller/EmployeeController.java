package com.example.demo.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Employee;
import com.example.demo.service.EmployeeService;

@RestController
@RequestMapping("/employee")
public class EmployeeController {
	
	@Autowired
	EmployeeService empService;
	
	//Create
	@PostMapping("/add")
	public String CreateEmployee(@RequestBody Map<String, Object> payload) {
		
		Employee e = new Employee();
		e.setEmployeename(payload.get("employeename").toString());
		e.setCity(payload.get("city").toString());
		empService.CreateEmployee(e);
		return "New Employee Created";	
	}
	
	//Read
	@GetMapping("/view")
	public List<Employee> DisplayAllEmployee() {
		return empService.DisplayAllEmployee();
	}
	
	//Update
	@PutMapping("/modify/{id}")
	public String UpdateEmployee(@PathVariable Integer id,@RequestBody Map<String, Object> payload) {
		
		Employee e = new Employee();
		e.setEmployeename(payload.get("employeename").toString());
		e.setCity(payload.get("city").toString());
		empService.UpdateEmployee(id, e);
		return "Employee Details Updated";
	}
	
	//Delete
	@DeleteMapping("/remove/{id}")
	public String DeleteEmployee(@PathVariable Integer id) {
		empService.DeleteEmployee(id);
		return "Employee Details Deleted";
	}
	

}
