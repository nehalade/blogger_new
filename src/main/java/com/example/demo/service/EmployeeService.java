package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Employee;
import com.example.demo.repository.EmployeeRepository;

@Service
public class EmployeeService {
	
	@Autowired
	EmployeeRepository empRepo;
	
	//Create
	public Employee CreateEmployee(Employee emp) {
		return empRepo.save(emp);
	}
	
	//Read
	public List<Employee> DisplayAllEmployee() {
		return empRepo.findAll();
	}
	
	//Update
	public Employee UpdateEmployee(Integer id,Employee emp) {
		Employee e = empRepo.findById(id).get();
		e.setEmployeename(emp.getEmployeename());
		e.setCity(emp.getCity());
		return empRepo.save(e);
	}
	
	//Delete
	public void DeleteEmployee(Integer id) {
		 empRepo.deleteById(id);
	}
	

}
